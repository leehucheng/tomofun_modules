import os
import glob
import json
from tqdm import tqdm
from pprint import pprint
from collections import defaultdict
import pickle
from shutil import copyfile
import csv
import json
import pickle


LABEL_TABLE = {
    'Barking':1,
    'Howling':2,
    'Crying':3,
    'CO_Smoke_Alert':4,
    'JP_Alert':4,
    'Glass_Breaking':5,
    'screaming':6,
    'door_slam':9,
    'Cat_Meow':10,
    'Cat_Fighting':11,
    'Cat_Crying':12
}

LABEL_NUM = {
    'AdvancedBarking':{'Barking':0,'Howling':1,'Crying':2},
    'HomeEmergency':{'CO_Smoke_Alert':0,'JP_Alert':0,'HomeEmergency':0,'JP_Alert':0,'JP_HomeEmergency':0},
    'GlassBreaking':{'GlassBreaking':0,'Glass_Breaking':0},
    'JP_HomeEmergency':{'CO_Smoke_Alert':0,'JP_Alert':0,'HomeEmergency':0,'JP_Alert':0,'JP_HomeEmergency':0},
    'FCN':{'Cat_Meow':0,'Cat_Fighting':1,'Cat_Crying':2,'cat':0}
}

class TaggingConverter:
    """Create tagging converter and save the audio/meta files to target directory.
    
    Attributes:
        taggings: A dict containing the audio taggings.
            taggings['cls']: taggings by cls_name. e.g. {'Barking': {'/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C0908896C7_1619911549_0.wav',
                                                                     '/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C09088977F_1619831036_0.wav',}}
            taggings['filepath']: taggings by audio file path. e.g. {'/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C0908896C7_1619911549_0.wav': {'Barking'},
                                                                     '/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C09088977F_1619831036_0.wav': {'Barking',
                                                                                                                                                 'Crying'},}
            
            taggings['filename']: taggings by audio file path. e.g. {'B0C0908896C7_1619911549_0.wav': ['Barking'],
                                                                     'B0C09088977F_1619831036_0.wav': ['Barking', 'Crying'],}
            taggings['meta_json']: taggings by json format as Tomofun Wiki listed. e.g. {'data': [{'EventId': '',
                                                                                                   'extras': {'remarks': ['Barking']},
                                                                                                   'label': '1',
                                                                                                   'log_src': '',
                                                                                                   'path': '/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C0908AF06D_1619881512_0.wav',
                                                                                                   'src': 'Furbo2.5'},
                                                                                                  {'EventId': '',
                                                                                                   'extras': {'remarks': ['Barking']},
                                                                                                   'label': '1',
                                                                                                   'log_src': '',
                                                                                                   'path': '/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C0908AE38D_1619834706_0.wav',
                                                                                                   'src': 'Furbo2.5'}]}
            taggings['meta_csv']: taggings by csv format as Model Training needed. e.g. [['/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C0908AF06D_1619881512_0.wav',
                                                                                          ['Barking']],
                                                                                         ['/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C0908AE38D_1619834706_0.wav',
                                                                                          ['Barking']]]
        info: A str containing tagging information. e.g. "Furbo NoEvent Tagging in 2021-10-21"
        device: A str indicating the device we used. e.g. "Furbo2.5"

    Methods:
        initialize_taggings(): initilize the taggings.
        read_taggings(str): read audio taggings from json file and convert them into taggings.
        save_json(str): save taggings as json format to target directory.
        save_csv(str): save taggings as csv format to target directory.
        save_dict(str): save taggings as dict by filename to target directory.
        read_json(str): read taggings from json format.
        read_dict(str): read taggings from pickle.
        copy_by_cls(str): copy audio files to target directory by class name.
    """
    def __init__(self, info='tagging information', device='Furbo2.5', meta_name=None):
        self.taggings = defaultdict(dict)
        self.info = info
        self.device = device
        self.meta_name = meta_name if meta_name is not None else ""
        self.initialize_taggings()
    
    def initialize_taggings(self):
        self.taggings['cls'] = defaultdict(set)
        self.taggings['filename'] = defaultdict(list)
        self.taggings['filepath'] = defaultdict(list)
        self.taggings['meta_json'] = {}
        self.taggings['meta_csv'] = []
        self.taggings['meta_json']['data'] = []
        self.taggings['meta_json']['meta'] = self.info
    
    def read_taggings(self, target_dir):
        """Read audio taggings from json file and convert them into taggings.
        Args:
            target_dir: the target directory of tagging json files
        Return:
            self.taggings['cls']
            self.taggings['filepath']
            self.taggings['filename']
            self.taggings['meta_json']
            self.taggings['meta_csv']
        """
        target_dir = os.path.join(target_dir, 'completions')
        labels = sorted(glob.glob(os.path.join(target_dir, '*.json')))
        for label in tqdm(labels):
            with open(label) as f:
                data = json.load(f)
            if 'task_path' in data:
                task_path = data['task_path']
            else:
                task_path = data["data"]["task_path"]
            if len(data['completions'][0]['result']) > 0:
                if 'choices' in data['completions'][0]['result'][0]['value']:
                    tag = data['completions'][0]['result'][0]['value']['choices']
                    tmp = {}
                    tmp["label"] = None
                    for t in tag:
                        if t == 'Other':
                            try:
                                t = data['completions'][0]['result'][1]['value']['text'][0]
                                # replace 'Other' to text remark
                                idx = tag.index('Other')
                                tag[idx] = t
                            except:
                                t = 'Other'
                        self.taggings['cls'][t].add(task_path)
                        self.taggings['filepath'][task_path].append(t)
                        self.taggings['filename'][os.path.basename(task_path)].append(t)
                        if t in LABEL_TABLE:
                            if tmp["label"] is None:
                                tmp["label"] = str(LABEL_TABLE[t])
                            else:
                                tmp["label"] += ",{}".format(str(LABEL_TABLE[t]))
                    tmp["label"] = 0 if tmp["label"] is None else tmp["label"]
                    tmp["path"] = task_path
                    tmp["src"] = self.device
                    tmp["log_src"] = ""
                    tmp["EventId"] = ""
                    tmp["extras"] = {}
                    tmp["extras"]["remarks"] = tag
                    self.taggings['meta_json']['data'].append(tmp)
                    self.taggings['meta_csv'].append([task_path, tag])
    
    def save_json(self, save_dir):
        """Save taggings as json file by cls name and all taggings.
        Args:
            save_dir: the target directory of saved tagging json files
        Return:
            cls_name.json
            meta.json
        """
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)
        
        # save individual json by cls
        for cls_name, paths in self.taggings['cls'].items():
            target_path = os.path.join(save_dir,'{}.json'.format(cls_name))
            tmp = dict()
            tmp['data'] = []
            for path in list(paths):
                t = {}
                t["label"] = 0 if cls_name not in LABEL_TABLE else LABEL_TABLE[cls_name]
                t["path"] = path
                t["src"] = self.device
                t["log_src"] = ""
                t["EventId"] = ""
                t["extras"] = {}
                t["extras"]["remarks"] = list(self.taggings['filepath'][path])
                tmp['data'].append(t)
            tmp['meta'] = self.info
            # if json file does not exist, then create one. Or append to existing json file.
            if not os.path.exists(target_path):
                with open(target_path,'w') as f:
                    json.dump([tmp], f)
            else:
                with open(target_path) as f:
                    data = json.load(f)
                data.append(tmp)
                with open(target_path,'w') as f:
                    json.dump(data, f)
        
        # save all meta in one json
        if not os.path.exists(os.path.join(save_dir,'meta.json')):
            with open(os.path.join(save_dir,'meta.json'),'w') as f:
                json.dump([self.taggings['meta_json']], f)
        else:
            with open(os.path.join(save_dir,'meta.json')) as f:
                data = json.load(f)
            data.append(self.taggings['meta_json'])
            with open(os.path.join(save_dir,'meta.json'),'w') as f:
                json.dump(data, f)
    
    def save_csv(self, save_dir, feature = None):
        """Save taggings as csv file by filepath.
        Args:
            save_dir: the target directory of saved tagging json files
        Return:
            meta.csv
            cls_name.csv
        """
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)
            
        if feature in ['AdvancedBarking','HomeEmergency','GlassBreaking','JP_HomeEmergency','FCN']:
            target_path = os.path.join(save_dir, '{}_{}.csv'.format(feature, self.meta_name))
            with open(target_path, 'w') as csvfile:
                writer = csv.writer(csvfile)
                writer.writerow(['path','label','remark'])
                for path, label in self.taggings['meta_csv']:
                    lb_num = 1 if feature in ['HomeEmergency','GlassBreaking','JP_HomeEmergency'] else 3
                    for t in label:
                        if t in LABEL_NUM[feature]:
                            lb_num = LABEL_NUM[feature][t]
                            break
                    writer.writerow([path, lb_num, label])
        else:
            target_path = os.path.join(save_dir, 'meta_{}.csv'.format(self.meta_name))
            with open(target_path, 'w') as csvfile:
                writer = csv.writer(csvfile)
                writer.writerow(['path','remark'])
                for path, label in self.taggings['meta_csv']:
                    writer.writerow([path, label])
    
    def save_dict(self, save_dir):
        """Save taggings as pickle file by filepath and filename.
        Args:
            save_dir: the target directory of saved tagging json files
        Return:
            meta_filepath.pkl
            meta_filename.pkl
        """
        with open(os.path.join(save_dir, 'meta_filepath_{}.pkl'.format(self.meta_name)), 'wb') as f:
            pickle.dump(self.taggings['filepath'], f)
        
        with open(os.path.join(save_dir, 'meta_filename_{}.pkl'.format(self.meta_name)), 'wb') as f:
            pickle.dump(self.taggings['filename'], f)
    
    def read_json(self, target_json):
        """Read taggings from json file.
        Args:
            target_json: the target json file of saved taggings
        Return:
            list
        """
        with open(target_json, 'r') as f:
            data = json.load(f)
        return data
    
    def read_dict(self, target_pkl):
        """Read taggings from pickle file.
        Args:
            target_pkl: the target pickle file of saved taggings
        Return:
            dict
        """
        with open(target_pkl, 'rb') as f:
            data = pickle.load(f)
        return data

    def copy_by_cls(self, save_dir):
        """Copy audio files to target dir by cls taggings.
        Args:
            save_dir: the target directory of saved audio files
        Return:
            saved_audio
        """
        for cls_name, paths in self.taggings['cls'].items():
            paths = list(paths)
            target_dir = os.path.join(save_dir, cls_name)
            if not os.path.exists(target_dir):
                os.makedirs(target_dir)
            for path in tqdm(paths):
                basename = os.path.basename(path)
                new_path = os.path.join(target_dir, basename)
                if not os.path.exists(new_path):
                    copyfile(path, new_path)
