# Tomofun Modules - sound utils

This repo contains the module that reading the audio taggings / save taggings / copy audio files.

## usage

```
from tomofun_modules.audio_utils import TaggingConverter

# declaration of taggings information and device information
# taggings will be re-initialized if TaggingConverter is declared again
tc = TaggingConverter(info='Furbo Barking Taggings 2021-05-01', device='Furbo 2.5')

# read tagggings in 2021-05-01 Barking
tc.read_taggings('/KIKI/hucheng/audio/tags/2021-05-01/Barking')

# save json files by [class_name].json & all taggings in meta.json in target directory
# generate Barking.json, meta.json, ..., etc in '/home/leehucheng/test_label'
####################################################
# {'data': [{'EventId': '',
#            'extras': {'remarks': ['Barking']},
#            'label': '1',
#            'log_src': '',
#            'path': '/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C0908AF06D_1619881512_0.wav',
#            'src': 'Furbo 2.5'},
#            ...}]
#  'meta': 'Furbo Barking Taggings 2021-05-01'
# }
tc.save_json('/home/leehucheng/test_label')

# save csv files with all taggings in meta.csv in target directory
# generate meta.csv in '/home/leehucheng/test_label'
####################################################
# path, remark
# '/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C0908AF06D_1619881512_0.wav', ['Barking']
# '/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C090D5F789_1619909134_0.wav', ['Barking', 'talking']
# ...
tc.save_csv('/home/leehucheng/test_label')

# save csv files with all taggings and barking labels in Barking.csv in target directory
# generate AdvancedBarking.csv in '/home/leehucheng/test_label'
####################################################
# path, label, remark
# '/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C0908AF06D_1619881512_0.wav', 0, ['Barking']
# '/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C090D5F789_1619909134_0.wav', 0, ['Barking', 'talking']
# ...
tc.save_csv('/home/leehucheng/test_label', feature='AdvancedBarking')

# save pickle files with all taggings by filepath and filename in target directory
# generate meta_filepath.pkl and meta_filename.pkl in '/home/leehucheng/test_label'
####################################################
{'/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C0908896C7_1619911549_0.wav': ['Barking'],
 '/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C09088977F_1619831036_0.wav': ['Barking','Crying'],
 ...
}
tc.save_dict('/home/leehucheng/test_label')

# read json file 
####################################################
# {'data': [{'EventId': '',
#            'extras': {'remarks': ['Barking']},
#            'label': '1',
#            'log_src': '',
#            'path': '/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C0908AF06D_1619881512_0.wav',
#            'src': 'Furbo 2.5'},
#            ...}]
#  'meta': 'Furbo Barking Taggings 2021-05-01'
# }
json_taggings = tc.read_json('/home/leehucheng/test_label/meta.json')

# read picke file
####################################################
{'/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C0908896C7_1619911549_0.wav': ['Barking'],
 '/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C09088977F_1619831036_0.wav': ['Barking','Crying'],
 ...
}
dict_taggings = tc.read_dict('/home/leehucheng/test_label/meta_filename.pkl')

# copy audio files classified by class name to target directory
# generate directories of Barking, ..., etc and copy corresponding audio files to those directories in '/home/leehucheng/test_label'
tc.copy_by_cls('/home/leehucheng/test_label')

```

## comment

```
Create tagging converter and save the audio/meta files to target directory.
    
    Attributes:
        taggings: A dict containing the audio taggings.
            taggings['cls']: taggings by cls_name. e.g. {'Barking': {'/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C0908896C7_1619911549_0.wav',
                                                                     '/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C09088977F_1619831036_0.wav',}}
            taggings['filepath']: taggings by audio file path. e.g. {'/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C0908896C7_1619911549_0.wav': {'Barking'},
                                                                     '/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C09088977F_1619831036_0.wav': {'Barking',
                                                                                                                                                 'Crying'},}
            
            taggings['filename']: taggings by audio file path. e.g. {'B0C0908896C7_1619911549_0.wav': ['Barking'],
                                                                     'B0C09088977F_1619831036_0.wav': ['Barking', 'Crying'],}
            taggings['meta_json']: taggings by json format as Tomofun Wiki listed. e.g. {'data': [{'EventId': '',
                                                                                                   'extras': {'remarks': ['Barking']},
                                                                                                   'label': '1',
                                                                                                   'log_src': '',
                                                                                                   'path': '/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C0908AF06D_1619881512_0.wav',
                                                                                                   'src': 'Furbo2.5'},
                                                                                                  {'EventId': '',
                                                                                                   'extras': {'remarks': ['Barking']},
                                                                                                   'label': '1',
                                                                                                   'log_src': '',
                                                                                                   'path': '/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C0908AE38D_1619834706_0.wav',
                                                                                                   'src': 'Furbo2.5'}]}
            taggings['meta_csv']: taggings by csv format as Model Training needed. e.g. [['/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C0908AF06D_1619881512_0.wav',
                                                                                          ['Barking']],
                                                                                         ['/mnt/NAS/hucheng/audio/2021-05-01/Barking/B0C0908AE38D_1619834706_0.wav',
                                                                                          ['Barking']]]
        info: A str containing tagging information. e.g. "Furbo NoEvent Tagging in 2021-10-21"
        device: A str indicating the device we used. e.g. "Furbo2.5"

    Methods:
        initialize_taggings(): initilize the taggings.
        read_taggings(str): read audio taggings from json file and convert them into taggings.
        save_json(str): save taggings as json format to target directory.
        save_csv(str): save taggings as csv format to target directory.
        save_dict(str): save taggings as dict by filename to target directory.
        read_json(str): read taggings from json format.
        read_dict(str): read taggings from pickle.
        copy_by_cls(str): copy audio files to target directory by class name.
```
